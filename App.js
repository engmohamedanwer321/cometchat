import React, {useEffect,useState} from 'react';
import {
  TouchableOpacity,View,Text,
} from 'react-native';
import { CometChat } from "@cometchat-pro/react-native-chat"


const App = () => {

  const [user,setUser] = useState(null)
  // call object of sender
  const [senderCallObj,setSenderCallObj] = useState(null)
  // call object of receiver
  const [receiverCallObj,setReceiverCallObj] = useState(null)
  // call object of receiver
  const [callSettings,setCallSettings] = useState(null)

  /******************************init********************************/

  /* step1: init comet chat */ 
  const initCometChat = () => {
    var appId = "27326f79c5fec65";
    let cometChatSettings = new CometChat.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion('us').build();
    CometChat.init(appId, cometChatSettings)
    .then(
      () => {
        console.log("Initialization completed successfully");
        //You can now call login function.
      },
      error => {
        console.log("Initialization failed with error:", error);
        //Check the reason for error and take apppropriate action.
      }
    )
  }

  /******************************auth********************************/

  /* step2: create user */ 
  const createUser = () => {

    var authKey = "3fef5f0550aefe685f56b3a0b342d4a25329e868";

    var user = new CometChat.User("user1");
    user.setName("user1");
    CometChat.createUser(user, authKey)
    .then(
      user => {
            console.log("user created", user);
      },error => {
            console.log("error", error);
            if(error.details.uid){
              console.log("THis USER REGISTERED BEFORE",);
            }
      }
    )

  }

  /* step3: login user  ( المروض اعمل لوجن بعد الرجيستر علي طول علشان اللوجن هيرجعلي ال الاوث توكن بتاع اليوزر وللاسف ده بيرجع في اللوجن بس مش في الرجيستر )*/ 
  const loginUser = (id) => {

    var authKey = "3fef5f0550aefe685f56b3a0b342d4a25329e868";
    var userID = 'user1'
    CometChat.login(id, authKey)
    .then(
      user => {
        console.log("Login Successful:", user );    
      },
      error => {
        console.log("Login failed with exception:", error);
        if(error.code=='ERR_UID_NOT_FOUND'){
          console.log("THIS USER NOT REGISTERED")
        }  
      }
    );

  }

  /******************************calling (default call)********************************/

  const makeCall = () => {

    var receiverID = "superhero5";
    var callType = CometChat.CALL_TYPE.VIDEO;
    var receiverType = CometChat.RECEIVER_TYPE.USER;

    var call = new CometChat.Call(receiverID, callType, receiverType);

    CometChat.initiateCall(call)
    .then(
      outGoingCall => {
        console.log("Call initiated successfully:", outGoingCall);
        setSenderCallObj(outGoingCall)
        // perform action on success. Like show your calling screen.
      },
      error => {
        console.log("Call initialization failed with exception:", error);
      }
    );

  }

  const receiveCall = () => {

    var receiverID = "superhero5";

    var listnerID = "superhero5";
    CometChat.addCallListener(
      listnerID,
      new CometChat.CallListener({
        onIncomingCallReceived(call) {
          console.log("Incoming call:", call);
          setReceiverCallObj(call)
          // Handle incoming call
        },
        onOutgoingCallAccepted(call) {
          console.log("Outgoing call accepted:", call);
          
          
          // Outgoing Call Accepted
        },
        onOutgoingCallRejected(call) {
          console.log("Outgoing call rejected:", call);
          // Outgoing Call Rejected
        },
        onIncomingCallCancelled(call) {
          console.log("Incoming call calcelled:", call);
        }
      })
    )

  }

  const acceptCall = () => {
    var sessionID = receiverCallObj.sessionId;
    CometChat.acceptCall(sessionID).then(
      call => {
        console.log("Call accepted successfully:", call);
        // start the call using the startCall() method
        startCall(call)
      },
      error => {
        console.log("Call acceptance failed with error", error);
        // handle exception
      }
    );
  }

  const rejectCall = () => {
    var sessionID = receiverCallObj.sessionId;
    var status = CometChat.CALL_STATUS.REJECTED;
    CometChat.rejectCall(sessionID, status).then(
      call => {
        console.log("Call rejected successfully", call);
      },
      error => {
        console.log("Call rejection failed with error:", error);
      }
    )
  }

  const canceltCall = () => {
    var sessionID = senderCallObj.sessionId;
    var status = CometChat.CALL_STATUS_CANCELLED;
    CometChat.rejectCall(sessionID, status).then(
      call => {
        console.log("Call rejected successfully", call);
      },
      error => {
        console.log("Call rejection failed with error:", error);
      }
    )
  }

  const startCall = (call) => {

    var sessionId = call.sessionId;
    var callType = call.type;
    let callListener = new CometChat.OngoingCallListener({
        onUserJoined: user => {
            console.log('User joined call:', user);
        },
        onUserLeft: user => {
            console.log('User left call:', user);
        },
        onCallEnded: call => {
            console.log('Call ended listener', call);
        },
        onError: error => {
            console.log('Call Error: ', error);
        },
    });

    var callSettings = new CometChat.CallSettingsBuilder()
            .setSessionID(sessionId)
            .setCallEventListener(callListener)
            .build();

    setCallSettings(callSettings)    

  }



  useEffect(()=>{
    initCometChat()
    receiveCall()
  },[])

 


  return (
   <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
      
      <View style={{flex:1}} >
        {callSettings&&<CometChat.CallingComponent callsettings= {callSettings} />}
      </View>

      <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-around',width:'100%',marginBottom:20}} >
        
        <TouchableOpacity 
        onPress={()=>loginUser('user1')}
        style={{backgroundColor:'red',justifyContent:'center',alignItems:'center'}}>
          <Text>user1 login</Text>
        </TouchableOpacity>

        <TouchableOpacity 
        onPress={()=>loginUser('superhero5')}
        style={{backgroundColor:'blue',justifyContent:'center',alignItems:'center'}}>
          <Text>user2 login</Text>
        </TouchableOpacity>

        <TouchableOpacity 
        onPress={()=>makeCall()}
        style={{backgroundColor:'green',justifyContent:'center',alignItems:'center'}}>
          <Text>start call</Text>
        </TouchableOpacity>

        <TouchableOpacity 
        onPress={()=>acceptCall()}
        style={{backgroundColor:'blue',justifyContent:'center',alignItems:'center'}}>
          <Text>accept call</Text>
        </TouchableOpacity>

      </View>


      

      

   </View>
  );
};


export default App;
