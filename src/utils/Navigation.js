


import { Navigation } from "react-native-navigation";

export const push = (pageName,data) => {
    Navigation.push('AppStack', {
      component: {
        name: pageName,
        passProps:{
          data:data?data:null
        }
      }
    })
}

export const pop = () => {
  Navigation.pop('AppStack');
}