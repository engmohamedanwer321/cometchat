import {Dimensions, PixelRatio, Platform, StatusBar} from 'react-native';


const {roundToNearestPixel} = PixelRatio;

const decorateHeights = Platform.OS === 'android' ? StatusBar.currentHeight : 0;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height - decorateHeights;


const maxWidth = 420;
const maxHeight = 800;


export const responsiveWidth = w => roundToNearestPixel( (windowWidth* w) / 100 )
/*export const responsiveWidth = w => {
  var width=1;
  if(windowWidth<windowHeight){
    width=windowWidth
    console.log('windowWidth<windowHeight')
  }else{
    width=windowHeight
    console.log('windowWidth>windowHeight')
  }
  return wp(w)
}*/

export const responsiveHeight = h => roundToNearestPixel( (windowHeight* h) / 100 );
/*export const responsiveHeight = h => {
  var height=1;
  if(windowHeight>windowWidth){
    height=windowHeight
  }else{
    height=windowWidth
  }
  return roundToNearestPixel( (height*h) / 100 )
}*/

export const moderateScale = (size, factor = 0.5) => {
  const rw = Math.min(maxWidth, windowWidth) * (size / 100);
  return roundToNearestPixel(size + (rw - size) * factor);
};

export const responsiveFontSize = (f, factor = 0.5) => {
   const rw = Math.min(maxWidth, windowWidth) * (f / 100);
   return roundToNearestPixel(f + (rw - f) * factor);
};


console.log('aaaaaaaa  ',Dimensions.get('window').height)
export const checkHeight = (h) => Dimensions.get('window').height>h?true:false


