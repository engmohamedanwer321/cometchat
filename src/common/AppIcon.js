//
import React, { Component } from "react";
import { Text, View} from "react-native";

import AntDesignIcon from 'react-native-vector-icons/AntDesign'
import EntypoIcon from 'react-native-vector-icons/Entypo'
import EvilIcon from 'react-native-vector-icons/EvilIcons'
import FeatherIcon from 'react-native-vector-icons/Feather'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5'
import FontAwesome5ProIcon from 'react-native-vector-icons/FontAwesome5Pro'
import FontistoIcon from 'react-native-vector-icons/Fontisto'
import FoundationIcon from 'react-native-vector-icons/Foundation'
import IoniconsIcon from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIconsIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import MaterialIconsIcon from 'react-native-vector-icons/MaterialIcons'
import OcticonsIcon from 'react-native-vector-icons/Octicons'
 
const AppIcon = (props) => {
       /* props */
        const {type,name,style} = props
        return(
            type=='Feather'?
            <FeatherIcon name={name} style={style}  />
            :
            type=='AntDesign'?
            <AntDesignIcon name={name} style={style}  />
            :
            type=='Entypo'?
            <EntypoIcon name={name} style={style}  />
            :
            type=='EvilIcons'?
            <EvilIcon name={name} style={style}  />
            :
            type=='FontAwesome'?
            <FontAwesomeIcon name={name} style={style}  />
            :
            type=='FontAwesome5'?
            <FontAwesome5Icon name={name} style={style}  />
            :
            type=='FontAwesome5Pro'?
            <FontAwesome5ProIcon name={name} style={style}  />
            :
            type=='Fontisto'?
            <FontistoIcon name={name} style={style}  />
            :
            type=='Foundation'?
            <FoundationIcon name={name} style={style}  />
            :
            type=='Ionicons'?
            <IoniconsIcon name={name} style={style}  />
            :
            type=='MaterialCommunityIcons'?
            <MaterialCommunityIconsIcon name={name} style={style}  />
            :
            type=='MaterialIcons'?
            <MaterialIconsIcon name={name} style={style}  />
            :
            type=='Octicons'?
            <OcticonsIcon name={name} style={style}  />
            :
            <AntDesignIcon name='question' color='black' size={25} />
        )
}

export default AppIcon
