import React, {useEffect,useState} from 'react';
import {
  TouchableOpacity,View,Text,TextInput
} from 'react-native';
import { CometChat } from "@cometchat-pro/react-native-chat"
import {push} from '../utils/Navigation'


const Call = (props) => {

  const {data} = props
  
  const [showCancelButton,setShowCancelButton] = useState(false)
  const [callSetting,setCallSetting]=useState(null)
  const [callObj,setCallObj] = useState(null)

  let callListener = new CometChat.OngoingCallListener({
    onUserJoined: user => {
        console.log('User joined call:', user);
    },
    onUserLeft: user => {
        console.log('User left call:', user);
    },
    onCallEnded: call => {
        console.log('Call ended listener', call);
    },
    onError: error => {
        console.log('Call Error: ', error);
    },
  });

  var callSettings = new CometChat.CallSettingsBuilder()
  .setSessionID("OPMMM")
  .enableDefaultLayout(true)
  .showSwitchCameraButton(true)
  .showEndCallButton(true)
  .showAudioModeButton(true)
  .showMuteAudioButton(true)
  .showPauseVideoButton(true)
  .build();

  
  const initCall = () => {
    var receiverID = "superhero2";
    var callType = CometChat.CALL_TYPE.VIDEO;
    var receiverType = CometChat.RECEIVER_TYPE.USER;
    var call = new CometChat.Call(receiverID, callType, receiverType);
    CometChat.initiateCall(call)
    .then(
      outGoingCall => {
        console.log("Call initiated successfully:", outGoingCall);
        setCallObj(outGoingCall)
        // perform action on success. Like show your calling screen.
      },
      error => {
        console.log("Call initialization failed with exception:", error);
      }
    );

  }

  const canceltCall = () => {
    var sessionID = callObj.sessionId;
    var status = CometChat.CALL_STATUS_CANCELLED;
    CometChat.rejectCall(sessionID, status).then(
      call => {
        console.log("Call canceld successfully", call);
        setShowCancelButton(false)
      },
      error => {
        console.log("Call rejection failed with error:", error);
      }
    )
  }

  const receiveCall = () => {

    var listnerID = new Date().getMilliseconds; // id of receiver 
    CometChat.addCallListener(
      listnerID,
      new CometChat.CallListener({
        onIncomingCallReceived(call) {
          console.log("Incoming call:", call);
          setCallObj(call)
          // Handle incoming call
        },
        onOutgoingCallAccepted(call) {
          console.log("Outgoing call accepted:", call);
          
          
          // Outgoing Call Accepted
        },
        onOutgoingCallRejected(call) {
          console.log("Outgoing call rejected:", call);
          // Outgoing Call Rejected
        },
        onIncomingCallCancelled(call) {
          console.log("Incoming call calcelled:", call);
        }
      })
    )

  }

  const acceptCall = () => {
    var sessionID = callObj.sessionId;
    CometChat.acceptCall(sessionID).then(
      call => {
        console.log("Call accepted successfully:", call);
        // start the call using the startCall() method
        //startCall(call)
      },
      error => {
        console.log("Call acceptance failed with error", error);
        // handle exception
      }
    );
  }

  const rejectCall = () => {
    var sessionID = callObj.sessionId;
    var status = CometChat.CALL_STATUS.REJECTED;
    CometChat.rejectCall(sessionID, status).then(
      call => {
        console.log("Call Rejected successfully", call);
        //setShowCancelButton(false)
      },
      error => {
        console.log("Call rejection failed with error:", error);
      }
    )
  }

 
  useEffect(()=>{
    receiveCall()
  },[])

  return (
   <View style={{position: 'relative',backgroundColor:'white',flex:1,justifyContent:'center',alignItems:'center'}} >
      
      
      <TouchableOpacity 
      onPress={initCall}
      style={{backgroundColor:'black',justifyContent:'center',alignItems:'center',marginTop:30,width:100,height:30,borderRadius:10}} >
        <Text style={{color:'white'}} >Make Call</Text>
      </TouchableOpacity>

      
      <TouchableOpacity 
      onPress={canceltCall}
      style={{backgroundColor:'black',justifyContent:'center',alignItems:'center',marginTop:30,width:100,height:30,borderRadius:10}} >
        <Text style={{color:'white'}} >Cancel Call</Text>
      </TouchableOpacity>

      <TouchableOpacity 
      onPress={rejectCall}
      style={{backgroundColor:'black',justifyContent:'center',alignItems:'center',marginTop:30,width:100,height:30,borderRadius:10}} >
        <Text style={{color:'white'}} >reject Call</Text>
      </TouchableOpacity>
      

      {callSettings&&<CometChat.CallingComponent callsettings={callSettings} style={{borderColor:'red',borderWidth:1}} /> }

   </View>
  );
};

export default Call;
