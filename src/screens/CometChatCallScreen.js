import React, {useEffect,useState} from 'react';
import {
  TouchableOpacity,View,Text,TextInput,ImageBackground,Image
} from 'react-native';
import { CometChat } from "@cometchat-pro/react-native-chat"
import {push,pop} from '../utils/Navigation'
import {responsiveHeight, responsiveWidth, responsiveFontSize, moderateScale} from '../utils/Dimensions'
import {AppIcon} from '../common'
import  Sound from 'react-native-sound' ;


const CometChatCallScreen = (props) => {
  
  const {data} = props

  const callListener = new CometChat.OngoingCallListener({
    onUserJoined: user => {
        console.log('OngoingCallListener: User joined call:', user.getUid());
    },
    onUserLeft: user => {
        console.log('OngoingCallListener: User left call:', user.getUid());
    },
    onCallEnded: call => {
        console.log('OngoingCallListener: Call ended listener', call.getSessionId());
        this.gotoChat();
    },
  });

  const callSettings = new CometChat.CallSettingsBuilder()
  .setSessionID(data.sessionId)
  .enableDefaultLayout(true)
  .setCallEventListener(callListener)
  
  .build();

  return (
   <View style={{flex:1,backgroundColor:'white'}} >
     <CometChat.CallingComponent callsettings= {callSettings} onFailure = {(e)=>{console.log('error', e)}} />
   </View>
  );
};

export default CometChatCallScreen;
