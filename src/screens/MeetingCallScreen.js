import React, {useEffect,useState} from 'react';
import {
  TouchableOpacity,View,Text,TextInput,ImageBackground,Image
} from 'react-native';
import { CometChat } from "@cometchat-pro/react-native-chat"
import {push,pop} from '../utils/Navigation'
import {responsiveHeight, responsiveWidth, responsiveFontSize, moderateScale} from '../utils/Dimensions'
import {AppIcon} from '../common'
import  Sound from 'react-native-sound' ;
import { RNToasty } from 'react-native-toasty';

const MeetingCallScreen = (props) => {
  
  const {data} = props

  const callListener = new CometChat.OngoingCallListener({
    onCallEnded: call => {
      console.log("Call ended:", call);
      pop()
    },
    onError: error => {
        console.log('Call Error: ', error);
    },
  });

  const callSettings = new CometChat.CallSettingsBuilder()
  .setSessionID(data.sessionId)
  .enableDefaultLayout(true)
  .setCallEventListener(callListener)
  .setIsAudioOnlyCall(false)
  .build();


  return (
    <View style={{flex:1,backgroundColor:'white'}} >
     <CometChat.CallingComponent callsettings= {callSettings} onFailure = {(e)=>{console.log('error', e)}} />
    </View>
  );
};

export default MeetingCallScreen;
