import React, {useEffect,useState} from 'react';
import {
  TouchableOpacity,View,Text,TextInput,ImageBackground,Image
} from 'react-native';
import { CometChat } from "@cometchat-pro/react-native-chat"
import {push,pop} from '../utils/Navigation'
import {responsiveHeight, responsiveWidth, responsiveFontSize, moderateScale} from '../utils/Dimensions'
import {AppIcon} from '../common'
import  Sound from 'react-native-sound' ;
import { RNToasty } from 'react-native-toasty';

const StartMeeting = (props) => {

  const {data} = props
  const [id,setId] = useState(null)

  const joinMeeting = () =>{
    CometChat.acceptCall(id)
    .then(call=>{
      push('MeetingCallScreen',{
        sessionId:id
      })
    })
    .catch(error=>{
      console.log("ERROR   ",error)
      RNToasty.Error({title:'seesion id is incorrect',duration:1})
    })
  }

  const initMeeting= (type) => {
    var receiverID = "superhero2";
    var callType = type=='video'? CometChat.CALL_TYPE.VIDEO:CometChat.CALL_TYPE.AUDIO;
    var receiverType = CometChat.RECEIVER_TYPE.USER;
    var call = new CometChat.Call(receiverID, callType, receiverType);
    CometChat.initiateCall(call)
    .then(
      outGoingCall => {
        console.log("Call initiated successfully:", outGoingCall);
        push('CallScreen',{
          ...outGoingCall,
        isOutGoingCall:true
        })
      },
      error => {
        console.log("Call initialization failed with exception:", error);
      }
    );

  }

  return (
    <View style={{flex:1,backgroundColor:'white',justifyContent:'center',alignItems:'center'}} >

        
      <TextInput 
      placeholder='ENTER SEESION ID'
      style={{marginTop:moderateScale(30), width:responsiveWidth(80),borderBottomWidth:1}}
      onChangeText={(val)=>setId(val)}
      />

        <TouchableOpacity 
        onPress={()=>{
          if(data.type=='teacher'){
            push('MeetingCallScreen',{
              sessionId:id
            })
          }else{
            joinMeeting()
          }
        }}
        style={{justifyContent:'center',alignItems:'center',backgroundColor:'black', marginTop:moderateScale(30),width:responsiveWidth(80),height:responsiveHeight(7),borderRadius:moderateScale(10)}} >
          <Text style={{fontSize:responsiveFontSize(6),color:'white'}} >{data.type=='teacher'?"START":'JOIN'}</Text>
        </TouchableOpacity>
     
    </View>
  );
};

export default StartMeeting;
