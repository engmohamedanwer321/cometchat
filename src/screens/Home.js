import React, {useEffect,useState} from 'react';
import {
  TouchableOpacity,View,Text,TextInput
} from 'react-native';
import { CometChat } from "@cometchat-pro/react-native-chat"
import {push} from '../utils/Navigation'
import {responsiveHeight, responsiveWidth, responsiveFontSize, moderateScale} from '../utils/Dimensions'
import {check,checkMultiple,requestMultiple, PERMISSIONS, RESULTS} from 'react-native-permissions';

const Home = (props) => {

  const {data} = props
  
  const [callObj,setCallObj] = useState(null)

  useEffect(()=>{
    checkPermessions()
    receiveCallListener()
    return () => {
      CometChat.removeCallListener('CALL_LISTENER_HOME');
    }
  },[])

  const receiveCallListener = () => {

    const listnerID = 'CALL_LISTENER_HOME'
    CometChat.addCallListener(
      listnerID,
      new CometChat.CallListener({
        onIncomingCallReceived(call) {
          console.log("Incoming call00:", call);
          if(call.receiverType=="user"){
            push('CallScreen',{
              ...call,
              isOutGoingCall:false
            })
          }
          //setCallObj(call)
        },
      })
    )
  }

  const checkPermessions = () => {
    checkMultiple([PERMISSIONS.ANDROID.CAMERA,PERMISSIONS.ANDROID.RECORD_AUDIO,PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE])
    .then((statuses) => {
      for (var key in statuses) {
          if (statuses.hasOwnProperty(key)) {
              switch (statuses[key]) {
                  case RESULTS.UNAVAILABLE:
                      break;
                  case RESULTS.DENIED:
                      requestMultiple([PERMISSIONS.ANDROID.CAMERA,PERMISSIONS.ANDROID.RECORD_AUDIO,PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE]).then((result) => {
                      });
                      break;
                  case RESULTS.GRANTED:
                      break;
                  case RESULTS.BLOCKED:
                      break;
              }
          }
      }
    })
    .catch((error) => {
        console.log('The permission error',error);
    });
  }



  return (
   <View style={{backgroundColor:'white',flex:1,justifyContent:'center',alignItems:'center'}} >
      
      <Text style={{fontFamily:'bold',fontSize:responsiveFontSize(20),color:'black'}} >HOME PAGE</Text>
      <TouchableOpacity 
      onPress={()=>push("Chat")}
      style={{justifyContent:'center',alignItems:'center',backgroundColor:'black', marginTop:moderateScale(30),width:responsiveWidth(80),height:responsiveHeight(7),borderRadius:moderateScale(10)}} >
        <Text style={{fontSize:responsiveFontSize(6),color:'white'}} >GO TO CHAT PAGE</Text>
      </TouchableOpacity>

      <TouchableOpacity 
      onPress={()=>push('StartMeeting',{
        type:'teacher',
      })}
      style={{justifyContent:'center',alignItems:'center',backgroundColor:'black', marginTop:moderateScale(10),width:responsiveWidth(80),height:responsiveHeight(7),borderRadius:moderateScale(10)}} >
        <Text style={{fontSize:responsiveFontSize(6),color:'white'}} >TEACGHER MEETINGS</Text>
      </TouchableOpacity>

      <TouchableOpacity 
       onPress={()=>push('StartMeeting',{
        type:'student',
      })}
      style={{justifyContent:'center',alignItems:'center',backgroundColor:'black', marginTop:moderateScale(10),width:responsiveWidth(80),height:responsiveHeight(7),borderRadius:moderateScale(10)}} >
        <Text style={{fontSize:responsiveFontSize(6),color:'white'}} >STUDENT MEETINGS</Text>
      </TouchableOpacity>

   </View>
  );
};

export default Home;
