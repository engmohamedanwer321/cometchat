
import { Navigation } from "react-native-navigation";
import Login from './Login'
import Chat from './Chat'
import Home from './Home'
import CallScreen from './CallScreen'
import CometChatCallScreen from './CometChatCallScreen'
import MeetingCallScreen from './MeetingCallScreen'
import StartMeeting from './StartMeeting'

export default function registerScreens() { 
    Navigation.registerComponent("Login", () => Login,);
    Navigation.registerComponent("Chat", () => Chat,);
    Navigation.registerComponent("Home", () => Home,);
    Navigation.registerComponent("CallScreen", () => CallScreen,);
    Navigation.registerComponent("CometChatCallScreen", () => CometChatCallScreen,);
    Navigation.registerComponent("MeetingCallScreen", () => MeetingCallScreen,);
    Navigation.registerComponent("StartMeeting", () => StartMeeting,);
}
