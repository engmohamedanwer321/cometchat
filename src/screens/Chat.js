import React, {useEffect,useState} from 'react';
import {
  TouchableOpacity,View,Text,TextInput
} from 'react-native';
import { CometChat } from "@cometchat-pro/react-native-chat"
import {push} from '../utils/Navigation'
import {responsiveHeight, responsiveWidth, responsiveFontSize, moderateScale} from '../utils/Dimensions'


const Chat = () => {

  const initCall = (type) => {
    var receiverID = "superhero2";
    var callType = type=='video'? CometChat.CALL_TYPE.VIDEO:CometChat.CALL_TYPE.AUDIO;
    var receiverType = CometChat.RECEIVER_TYPE.USER;
    var call = new CometChat.Call(receiverID, callType, receiverType);
    CometChat.initiateCall(call)
    .then(
      outGoingCall => {
        console.log("Call initiated successfully:", outGoingCall);
        push('CallScreen',{
          ...outGoingCall,
        isOutGoingCall:true
        })
      },
      error => {
        console.log("Call initialization failed with exception:", error);
      }
    );

  }

  return (
   <View style={{position: 'relative',backgroundColor:'white',flex:1,justifyContent:'center',alignItems:'center'}} >
      
      
      <Text style={{fontFamily:'bold',fontSize:responsiveFontSize(20),color:'black'}} >CHAT PAGE</Text>
      
      <TouchableOpacity 
      onPress={()=>initCall('video')}
      style={{justifyContent:'center',alignItems:'center',backgroundColor:'black', marginTop:moderateScale(30),width:responsiveWidth(80),height:responsiveHeight(7),borderRadius:moderateScale(10)}} >
        <Text style={{fontSize:responsiveFontSize(6),color:'white'}} >VIDEO CAll SUPERHERO2</Text>
      </TouchableOpacity>
      

      <TouchableOpacity 
      onPress={()=>initCall('audio')}
      style={{justifyContent:'center',alignItems:'center',backgroundColor:'black', marginTop:moderateScale(10),width:responsiveWidth(80),height:responsiveHeight(7),borderRadius:moderateScale(10)}} >
        <Text style={{fontSize:responsiveFontSize(6),color:'white'}} >AUDIO CAll SUPERHERO2</Text>
      </TouchableOpacity>

    
   </View>
  );
};

export default Chat;
