import React, {useEffect,useState} from 'react';
import {
  TouchableOpacity,View,Text,TextInput,ActivityIndicator
} from 'react-native';
import { CometChat, } from "@cometchat-pro/react-native-chat"
import {push} from '../utils/Navigation'
import {responsiveHeight, responsiveWidth, responsiveFontSize, moderateScale} from '../utils/Dimensions'


const Login = () => {
 
  const [id,setId] = useState(null)
  const [loading,setLoading] = useState(false)
  
  const loginUser = () => {
    setLoading(true)
    var authKey = "3fef5f0550aefe685f56b3a0b342d4a25329e868";
    CometChat.login(id, authKey)
    .then(
      user => {
        console.log("Login Successful:", user );
        push('Home',user)
        setLoading(false)
      },
      error => {
        setLoading(false)
        console.log("Login failed with exception:", error);
        if(error.code=='ERR_UID_NOT_FOUND'){
          console.log("THIS USER NOT REGISTERED")
        }  
      }
    );

  }

  return (
   <View style={{backgroundColor:'white',flex:1,justifyContent:'center',alignItems:'center'}} >
      

      <Text style={{fontFamily:'bold',fontSize:responsiveFontSize(20),color:'black'}} >LOGIN PAGE</Text>

      <TextInput 
      placeholder='Enter ID'
      style={{marginTop:moderateScale(30), width:responsiveWidth(80),borderBottomWidth:1}}
      onChangeText={(val)=>setId(val)}
      />

      <TouchableOpacity 
      onPress={loginUser}
      style={{justifyContent:'center',alignItems:'center',backgroundColor:'black', marginTop:moderateScale(15),width:responsiveWidth(80),height:responsiveHeight(7),borderRadius:moderateScale(10)}} >
        <Text style={{fontSize:responsiveFontSize(6),color:'white'}} >LOGIN USER</Text>
      </TouchableOpacity>

      {loading&&<ActivityIndicator color='black' size='large' style={{position:'absolute',bottom:moderateScale(20)}} />}

   </View>
  );
};

export default Login;
