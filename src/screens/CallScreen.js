import React, {useEffect,useState} from 'react';
import {
  TouchableOpacity,View,Text,TextInput,ImageBackground,Image
} from 'react-native';
import { CometChat } from "@cometchat-pro/react-native-chat"
import {push,pop} from '../utils/Navigation'
import {responsiveHeight, responsiveWidth, responsiveFontSize, moderateScale} from '../utils/Dimensions'
import {AppIcon} from '../common'
import  Sound from 'react-native-sound' ;
import { RNToasty } from 'react-native-toasty';

const CallScreen = (props) => {
  
  const {data} = props
  
  var callSound;
  const [isCallStarted,setIsCallStarted] = useState(false)

  const callListener2 = new CometChat.OngoingCallListener({
    onUserJoined: user => {
        console.log('OngoingCallListener: User joined call:', user.getUid());
        //RNToasty.Show({title:`${user.name} joined to call`,duration:1})
      },
    onUserLeft: user => {
        console.log('OngoingCallListener: User left call:', user.getUid());
        //RNToasty.Show({title:`${user.name} joined to call`,duration:1})
    },
    onCallEnded: call => {
        console.log('OngoingCallListener: Call ended listener', call.getSessionId());
        pop()
        RNToasty.Show({title:`تم انهاء المكالمه`})
    },
  });
  const callSettings = new CometChat.CallSettingsBuilder()
  .setSessionID(data.sessionId)
  .enableDefaultLayout(true)
  .setCallEventListener(callListener2)
  .setIsAudioOnlyCall(data.type == 'aduio' ? true : false)
  .build();

  useEffect(()=>{
    callListener()
    playSound()
    return () => {
      callSound.stop()
      CometChat.removeCallListener('CALL_LISTENER_CALLING_SCREEN');
    }
  },[])

  const callListener = () => {
    var listnerID = 'CALL_LISTENER_CALLING_SCREEN'
    CometChat.addCallListener(
      listnerID,
      new CometChat.CallListener({
          onOutgoingCallAccepted(call) {
            console.log("CALL ACCEPTED")
            startCall(call)
          },
          onOutgoingCallRejected(call) {
            console.log("CALL REJECTED")
            RNToasty.Show({title:`تم انهاء المكالمه`})
            pop()
          },
          onIncomingCallCancelled(call) {
            console.log("CALL CANCELED")
            pop()
          },
      })
    );

  }

  const playSound = () => {
    Sound.setCategory('Playback');
    callSound = new Sound('call.mp3', Sound.MAIN_BUNDLE, (error) => {
        if (error) {
          console.log('failed to load the sound', error);
          return;
        }else{
          callSound.play((success) => {
                if (success) {
                  console.log('successfully finished playing');
                  callSound.play()
                } else {
                  console.log('playback failed due to audio decoding errors');
                }
            });
        }
    })
  }

  const cancelCall = () => {
    var sessionID = data.sessionId;
    var status = CometChat.CALL_STATUS.CANCELLED;
    CometChat.rejectCall(sessionID, status).then(
      call => {
        console.log("Call canceld successfully", call);
        pop()
      },
      error => {
        console.log("Call rejection failed with error:", error);
      }
    )
  }

  const rejectCall = () => {
    var sessionID = data.sessionId;
    var status = CometChat.CALL_STATUS.REJECTED;
    CometChat.rejectCall(sessionID, status).then(
      call => {
        console.log("Call rejected successfully", call);
        pop()
      },
      error => {
        console.log("Call rejection failed with error:", error);
      }
    )
  }

  const acceptCall = () =>{
    CometChat.acceptCall(data.sessionId)
    .then(call=>startCall(call))
  }

  const startCall = (call) => {
    //push('CometChatCallScreen',call)
    setIsCallStarted(true)
    callSound.stop()
  }

  return (
    isCallStarted?
    <View style={{flex:1,backgroundColor:'white'}} >
     <CometChat.CallingComponent callsettings= {callSettings} onFailure = {(e)=>{console.log('error', e)}} />
    </View>
    :
   <ImageBackground 
   source={require('../assets/images/callImage.jpg')}
   style={{flex:1,alignItems:'center'}} 
   >
      
      <Image 
      style={{borderWidth:2,borderColor:'white',marginTop:moderateScale(40),width:100,height:100,borderRadius:50}}
      source={{uri:'https://cdn.fastly.picmonkey.com/contentful/h6goo9gw1hh6/2sNZtFAWOdP1lmQ33VwRN3/24e953b920a9cd0ff2e1d587742a2472/1-intro-photo-final.jpg?w=800&q=70'}}
      />

      
      {data.isOutGoingCall?
      <TouchableOpacity
      onPress={cancelCall}
       style={{width:60,height:60,borderRadius:30,backgroundColor:'red',justifyContent:'center',alignItems:'center',position:'absolute',bottom:moderateScale(30)}} >
        <AppIcon name='phone' type='Entypo' style={{color:'white',fontSize:responsiveFontSize(10)}} />
      </TouchableOpacity>
      : 
      <View style={{position:'absolute',bottom:moderateScale(30),width:responsiveWidth(60),flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
        <TouchableOpacity
        onPress={rejectCall}
         style={{width:60,height:60,borderRadius:30,backgroundColor:'red',justifyContent:'center',alignItems:'center',}} >
          <AppIcon name='close' type='AntDesign' style={{color:'white',fontSize:responsiveFontSize(10)}} />
        </TouchableOpacity>

        <TouchableOpacity
        onPress={acceptCall}
         style={{width:60,height:60,borderRadius:30,backgroundColor:'green',justifyContent:'center',alignItems:'center',}} >
          <AppIcon name='phone' type='Entypo' style={{color:'white',fontSize:responsiveFontSize(10)}} />
        </TouchableOpacity>
      </View>
      }

   </ImageBackground>
  );
};

export default CallScreen;
