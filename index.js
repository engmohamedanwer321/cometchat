
import {AppRegistry} from 'react-native';

import { Navigation } from "react-native-navigation";
import registerScreens from './src/screens'
import { CometChat } from "@cometchat-pro/react-native-chat"



const initCometChat = () => {
  var appId = "27326f79c5fec65";
  let cometChatSettings = new CometChat.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion('us').build();
  CometChat.init(appId, cometChatSettings)
  .then(
    () => {
      console.log("Initialization completed successfully");
      //You can now call login function.
    },
    error => {
      console.log("Initialization failed with error:", error);
      //Check the reason for error and take apppropriate action.
    }
  )
}

initCometChat()

registerScreens()

Navigation.events().registerAppLaunchedListener(() => {

  Navigation.setDefaultOptions({
    topBar:{
      visible:false
    },
    layout:{
      backgroundColor:'white'
    }
  })

    Navigation.setRoot({
      root: {
        stack: {
            id: 'AppStack',
             children: [
               {
                   component: {
                       id: 'Login',
                       name: "Login",
                       options:{
                         statusBar:{
                           backgroundColor:'white'
                         }
                       }
                   }
               },
              
           ]
         },
      }
    });
  
    
});
  
  
  


